package com.ruoyi.project.module.upload.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 资讯上传对象 tb_news_upload
 * 
 * @author ruoyi
 * @date 2020-05-13
 */
public class NewsUpload extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 资讯id */
    @Excel(name = "资讯id")
    private Long newId;

    /** 上传文件地址 */
    @Excel(name = "上传文件地址")
    private String newFile;

    /** 上传视频地址 */
    @Excel(name = "上传视频地址")
    private String newVideo;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setNewId(Long newId) 
    {
        this.newId = newId;
    }

    public Long getNewId() 
    {
        return newId;
    }
    public void setNewFile(String newFile) 
    {
        this.newFile = newFile;
    }

    public String getNewFile() 
    {
        return newFile;
    }
    public void setNewVideo(String newVideo) 
    {
        this.newVideo = newVideo;
    }

    public String getNewVideo() 
    {
        return newVideo;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("newId", getNewId())
            .append("newFile", getNewFile())
            .append("newVideo", getNewVideo())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
