package com.ruoyi.project.module.deed.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 事迹对象 tb_deed
 * 
 * @author ruoyi
 * @date 2020-05-08
 */
public class Deed extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long deedId;

    /** 事迹标题 */
    @Excel(name = "事迹标题")
    private String deedTitle;

    /** 封面图 */
    @Excel(name = "封面图")
    private String deedPic;

    /** 事迹内容 */
    @Excel(name = "事迹内容")
    private String deedContent;

    /** 状态（0正常 1关闭） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=关闭")
    private String status;

    public void setDeedId(Long deedId) 
    {
        this.deedId = deedId;
    }

    public Long getDeedId() 
    {
        return deedId;
    }
    public void setDeedTitle(String deedTitle) 
    {
        this.deedTitle = deedTitle;
    }

    public String getDeedTitle() 
    {
        return deedTitle;
    }
    public void setDeedPic(String deedPic) 
    {
        this.deedPic = deedPic;
    }

    public String getDeedPic() 
    {
        return deedPic;
    }
    public void setDeedContent(String deedContent) 
    {
        this.deedContent = deedContent;
    }

    public String getDeedContent() 
    {
        return deedContent;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("deedId", getDeedId())
            .append("deedTitle", getDeedTitle())
            .append("deedPic", getDeedPic())
            .append("deedContent", getDeedContent())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
