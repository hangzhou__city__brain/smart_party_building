package com.ruoyi.project.module.inspect.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 视察对象 tb_inspect
 * 
 * @author ruoyi
 * @date 2020-05-07
 */
public class Inspect extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long inspectId;

    /** 视察标题 */
    @Excel(name = "视察标题")
    private String inspectTitle;

    /** 封面图 */
    @Excel(name = "封面图")
    private String inspectPic;

    /** 视察内容 */
    @Excel(name = "视察内容")
    private String inspectContent;

    /** 状态（0正常 1关闭） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=关闭")
    private String status;

    public void setInspectId(Long inspectId) 
    {
        this.inspectId = inspectId;
    }

    public Long getInspectId() 
    {
        return inspectId;
    }
    public void setInspectTitle(String inspectTitle) 
    {
        this.inspectTitle = inspectTitle;
    }

    public String getInspectTitle() 
    {
        return inspectTitle;
    }
    public void setInspectPic(String inspectPic) 
    {
        this.inspectPic = inspectPic;
    }

    public String getInspectPic() 
    {
        return inspectPic;
    }
    public void setInspectContent(String inspectContent) 
    {
        this.inspectContent = inspectContent;
    }

    public String getInspectContent() 
    {
        return inspectContent;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("inspectId", getInspectId())
            .append("inspectTitle", getInspectTitle())
            .append("inspectContent", getInspectContent())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
