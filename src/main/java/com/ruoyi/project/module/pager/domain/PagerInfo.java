package com.ruoyi.project.module.pager.domain;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;
import java.util.List;

/**
 * 试卷详情 tb_pager
 * 
 * @author ruoyi
 * @date 2020-06-05
 */
public class PagerInfo
{
    private static final long serialVersionUID = 1L;

    private Long questionId; //题目id
    private String title; //题目标题

    private Long type; //题目类型 （1.单选题  2.判断题 3 多选题 .4.简答题）

    private List<QuestionOption> optionList; //题目选项

    private String answer; //题目答案

    private String userAnswer; //用户填的答案

    private boolean userFavor; //是否收藏

    private String explain; //答案解析

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public void setTitle(String title){
        this.title = title;
    }
    public String getTitle(){
        return this.title;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public List<QuestionOption> getOptionList() {
        return optionList;
    }

    public void setOptionList(List<QuestionOption> optionList) {
        this.optionList = optionList;
    }

    public void setAnswer(String answer){
        this.answer = answer;
    }
    public String getAnswer(){
        return this.answer;
    }
    public void setUserAnswer(String userAnswer){
        this.userAnswer = userAnswer;
    }
    public String getUserAnswer(){
        return this.userAnswer;
    }
    public void setUserFavor(boolean userFavor){
        this.userFavor = userFavor;
    }
    public boolean getUserFavor(){
        return this.userFavor;
    }
    public void setExplain(String explain){
        this.explain = explain;
    }
    public String getExplain(){
        return this.explain;
    }

    public boolean isUserFavor() {
        return userFavor;
    }

   public static class QuestionOption{
        private String id;

        private String content;

        public void setId(String id){
            this.id = id;
        }
        public String getId(){
            return this.id;
        }
        public void setContent(String content){
            this.content = content;
        }
        public String getContent(){
            return this.content;
        }
    }

}

