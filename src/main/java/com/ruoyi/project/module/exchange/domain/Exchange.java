package com.ruoyi.project.module.exchange.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 交流对象 tb_exchange
 * 
 * @author ruoyi
 * @date 2020-05-08
 */
public class Exchange extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long exchangeId;

    /** 交流标题 */
    @Excel(name = "交流标题")
    private String exchangeTitle;

    /** 封面图 */
    @Excel(name = "封面图")
    private String exchangePic;

    /** 交流内容 */
    @Excel(name = "交流内容")
    private String exchangeContent;

    /** 状态（0正常 1关闭） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=关闭")
    private String status;

    public void setExchangeId(Long exchangeId) 
    {
        this.exchangeId = exchangeId;
    }

    public Long getExchangeId() 
    {
        return exchangeId;
    }
    public void setExchangeTitle(String exchangeTitle) 
    {
        this.exchangeTitle = exchangeTitle;
    }

    public String getExchangeTitle() 
    {
        return exchangeTitle;
    }
    public void setExchangePic(String exchangePic) 
    {
        this.exchangePic = exchangePic;
    }

    public String getExchangePic() 
    {
        return exchangePic;
    }
    public void setExchangeContent(String exchangeContent) 
    {
        this.exchangeContent = exchangeContent;
    }

    public String getExchangeContent() 
    {
        return exchangeContent;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("exchangeId", getExchangeId())
            .append("exchangeTitle", getExchangeTitle())
            .append("exchangeContent", getExchangeContent())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
