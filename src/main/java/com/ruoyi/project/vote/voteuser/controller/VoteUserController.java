package com.ruoyi.project.vote.voteuser.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.vote.voteuser.domain.VoteUser;
import com.ruoyi.project.vote.voteuser.service.IVoteUserService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 选举人Controller
 * 
 * @author ruoyi
 * @date 2020-07-01
 */
@Controller
@RequestMapping("/vote/voteuser")
public class VoteUserController extends BaseController
{
    private String prefix = "vote/voteuser";

    @Autowired
    private IVoteUserService voteUserService;

    @RequiresPermissions("vote:voteuser:view")
    @GetMapping()
    public String voteuser()
    {
        return prefix + "/voteuser";
    }

    /**
     * 查询选举人列表
     */
    @RequiresPermissions("vote:voteuser:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(VoteUser voteUser)
    {
        startPage();
        List<VoteUser> list = voteUserService.selectVoteUserList(voteUser);
        return getDataTable(list);
    }

    /**
     * 导出选举人列表
     */
    @RequiresPermissions("vote:voteuser:export")
    @Log(title = "选举人", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(VoteUser voteUser)
    {
        List<VoteUser> list = voteUserService.selectVoteUserList(voteUser);
        ExcelUtil<VoteUser> util = new ExcelUtil<VoteUser>(VoteUser.class);
        return util.exportExcel(list, "voteuser");
    }

    /**
     * 新增选举人
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存选举人
     */
    @RequiresPermissions("vote:voteuser:add")
    @Log(title = "选举人", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(VoteUser voteUser)
    {
        return toAjax(voteUserService.insertVoteUser(voteUser));
    }

    /**
     * 修改选举人
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        VoteUser voteUser = voteUserService.selectVoteUserById(id);
        mmap.put("voteUser", voteUser);
        return prefix + "/edit";
    }

    /**
     * 修改保存选举人
     */
    @RequiresPermissions("vote:voteuser:edit")
    @Log(title = "选举人", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(VoteUser voteUser)
    {
        return toAjax(voteUserService.updateVoteUser(voteUser));
    }

    /**
     * 删除选举人
     */
    @RequiresPermissions("vote:voteuser:remove")
    @Log(title = "选举人", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(voteUserService.deleteVoteUserByIds(ids));
    }
}
